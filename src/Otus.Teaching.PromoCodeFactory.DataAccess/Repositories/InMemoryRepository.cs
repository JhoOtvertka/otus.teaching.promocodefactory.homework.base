﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        
        public void AddElement(T element)
        {
            var data = Data as List<T>;
            data.Add(element);
            Data = data;
        }

        public Task UpdateElement(T element)
        {
            var data = Data as List<T>;
            var fromDb = data.FirstOrDefault(x => x.Id == element.Id);
            data.Remove(fromDb);
            data.Add(element);
            Data = data;

            return Task.CompletedTask;
        }

        public Task DeleteElement(Guid id)
        {
            var data = Data as List<T>;
            var fromDb = data.FirstOrDefault(x => x.Id == id);
            data.Remove(fromDb);
            Data = data;

            return Task.CompletedTask;
        }

    }
}