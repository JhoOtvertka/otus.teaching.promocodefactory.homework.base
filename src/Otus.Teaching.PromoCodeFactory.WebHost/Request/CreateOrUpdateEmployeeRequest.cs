﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Request
{
    public class CreateOrUpdateEmployeeRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Role> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }

        public Employee Map(Employee employee)
        {
            employee.FirstName = FirstName;
            employee.LastName = LastName;
            employee.Email = Email;
            employee.Roles = Roles;
            employee.AppliedPromocodesCount = AppliedPromocodesCount;
            return employee;
        }
    }
}
