﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Request;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создает нового сотрудника
        /// </summary>
        /// <param name="request">Класс запроса создания нового сотрудника</param>
        /// <returns></returns>
        [HttpPost]
        public Task CreateEmployee(CreateOrUpdateEmployeeRequest request)
        {
            var employee = request.Map(new Employee());
            employee.Id = Guid.NewGuid();
            
            _employeeRepository.AddElement(employee);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Редактирование данных сотрудника
        /// </summary>
        /// <param name="request">Класс запроса редактирование данных сотрудника</param>
        /// <param name="id">Уникальный идентификатор сотрудника</param>
        /// <returns></returns>
        [HttpPatch("{id:guid}")]
        public async Task UpdateEmployee(CreateOrUpdateEmployeeRequest request, Guid id)
        {
            var toUpdate = await GetEmployeeByIdAsync(id);
            if(toUpdate.Value is null)
            {
                throw new Exception($"Не удалось найти элемент с ID: {id}.");
            }
            var employee = request.Map(new Employee());
            employee.Id = id;
                        
            await _employeeRepository.UpdateElement(employee);
        }

        /// <summary>
        /// Удаление данных сотрудника
        /// </summary>
        /// <param name="id">Уникальный идентификатор сотрудника</param>
        [HttpDelete("{id:guid}")]
        public async Task DeleteElement(Guid id)
        {
            var toDelete = await GetEmployeeByIdAsync(id);
            if (toDelete.Value is null)
            {
                throw new Exception($"Не удалось найти элемент с ID: {id}.");
            }
            await _employeeRepository.DeleteElement(id);
        }

    }
}